'use strict';

requirejs.config({
    paths: {
        'app': 'app',
        'compartidas': 'js/shared',
        'jquery': 'plugins/jquery/jquery.min',
        'knockout': 'plugins/knockout/knockout-3.5.1',
        'koValidation': 'plugins/knockout/knockout.validation.min',
        'koEs': 'plugins/knockout/es-ES',
        'koUtilExtenders': 'plugins/knockout/knockout.util.extenders',
        'koMapping': 'plugins/knockout/knockout.mapping-latest',
        'text': 'plugins/require/text'
            /*,           'json3': 'assets/plugins/json3',
                        'xmlToJson': 'assets/plugins/xmlToJson'*/
    },
    shim: {
        jquery: {
            exports: '$'
        },
        app: {
            deps: ['jquery', 'compartidas'],
            exports: 'app'
        },
        knockout: {
            exports: 'ko'
        },
        koValidation: {
            deps: ['knockout']
        },
        compartidas: {
            deps: ['jquery']
        },
        koUtilExtenders: {
            deps: ['knockout']
        },
        komapping: {
            deps: ['knockout'],
            exports: 'koMapping'
        }
    }
});

require(['app', 'plugins/require/domReady'], function(app, domReady) {
    domReady(function() {
        app.Inicializar();
    });

});