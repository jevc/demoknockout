console.log("app.js cargada");

var _app;

define(['compartidas', 'jquery', 'knockout', 'koValidation', 'koEs', 'koUtilExtenders'],

    function(compartidas, $, ko) {

        function App(compartidas) {

            var self = this;

            //#region Propiedades                
            self.RutaViewModels = "../viewmodels/";
            self.RutaViews = "../views/";

            this.Inicializar = function() {

                CargarVistasGenerales();
                console.log("App.js inicializada");
            }

            var CargarVistasGenerales = function() {
                console.log('CargarVistasGenerales');

                require(["text!" + _app.RutaViews + "shared/header.html",
                        "text!" + _app.RutaViews + "shared/sidebar.html",
                        "text!" + _app.RutaViews + "shared/footer.html"
                    ],
                    function(viewHeader, sidebar, footer) {

                        $("#header").html(viewHeader);
                        $("#sidebar").html(sidebar);
                        $("#footer").html(footer);

                    }
                );

            }
        };

        _app = new App(compartidas);
        return _app;

    });