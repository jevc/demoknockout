/**
 * Localization file for Spanish - Spain (es-ES)
 */
(function(factory) {
    // Module systems magic dance.
    /*global require,ko.validation,define,module*/
    if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
        // CommonJS or Node
        module.exports = factory(require('../'));
    } else if (typeof define === 'function' && define['amd']) {
        // AMD anonymous module
        define(['koValidation'], factory);
    } else {
        // <script> tag: use the global `ko.validation` object
        factory(ko.validation);
    }
}(function(kv) {
    if (!kv || typeof kv.defineLocale !== 'function') {
        throw new Error('Knockout-Validation is required, please ensure it is loaded before this localization file');
    }
    return kv.defineLocale('es-ES', {
        required: 'Campo requerido',
        min: 'Introduzca un valor igual o mayor a {0}',
        max: 'Por favor, introduzca un valor menor o igual a {0}',
        minLength: 'Mínimo {0} datos',
        maxLength: 'Máximo {0} datos',
        pattern: 'Por favor, compruebe este campo',
        step: 'El valor debe incrementarse por {0}',
        email: 'Formato de email incorrecto',
        date: 'Por favor, introduzca una fecha correcta',
        dateISO: 'Por favor, introduzca una fecha correcta',
        number: 'Por favor, introduzca solo números',
        digit: 'Por favor, introduzca un dígito',
        phoneUS: 'Por favor, introduzca un número de teléfono válido para EEUU',
        equal: 'Los valores deben ser iguales',
        notEqual: 'Por favor, elija otro valor',
        unique: 'Por favor, asegurese de que el valor sea único'
    });
}));